package com.example.crucible.restservice;

import com.atlassian.crucible.spi.data.ReviewData;
import com.atlassian.crucible.spi.data.ReviewerData;
import com.atlassian.crucible.spi.data.UserData;
import com.atlassian.crucible.spi.services.ReviewService;
import com.sun.tools.javac.util.Pair;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.*;

@Path("/")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public class RestCompletionStatusService {
    private final ReviewService reviewService;

    public RestCompletionStatusService(ReviewService reviewService) {
        this.reviewService = reviewService;
    }

    // We can't return a naked Collection, it must be wrapped in a container
    @XmlRootElement
    public static class Reviewers {
        public Reviewers(Collection<ReviewerData> reviewer) {
            this.reviewer = reviewer;
        }

        // Needed by JAXB
        private Reviewers() {
        }

        // Must be public. By naming this field 'reviewer' each element in the Collection is
        // placed inside a '<reviewer>' element in the XML
        public Collection<ReviewerData> reviewer;
    }

    @GET
    @Path("users")
    public Response getUncompletedUsers() {
        Set<ReviewerData> users = new HashSet<ReviewerData>();
        for (ReviewData reviewData : reviewService.getFilteredReviews("allOpenReviews",false)) {
            System.out.println(reviewData.getPermaId());
            users.addAll(reviewService.getUncompletedReviewers(reviewData.getPermaId()));
        }
        return Response.ok(new Reviewers(users)).build();
    }
}
